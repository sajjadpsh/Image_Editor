package ir.ac.aut.image;



import ir.ac.aut.Image;
import ir.ac.aut.filter.Rotate;
import ir.ac.aut.graphics.GuiSetup;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;


public class OpenImage extends JPanel {

    public OpenImage(GuiSetup f){
        JFileChooser file = new JFileChooser();
        file.setCurrentDirectory(new File(System.getProperty("user.home")));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        file.addChoosableFileFilter(filter);
        int result = file.showOpenDialog(null);
        String Path = null;

        switch (result) {
            case JFileChooser.APPROVE_OPTION:
                Path = file.getSelectedFile().getAbsolutePath();
                break;
            case JFileChooser.CANCEL_OPTION:
                return;
        }



        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        JPanel panel = new JPanel();
        System.setProperty("user.home", Path);
        Image si = new Image(Path);


        JComboBox<String> choices = new JComboBox<String>(si.getDescriptions());
        choices.setActionCommand("SetFilter");
        choices.addActionListener(si);
        JComboBox<String> formats = new JComboBox<String>(si.getFormats());
        formats.setActionCommand("Formats");
        formats.addActionListener(si);

        si.setChoiceComponent(choices);
        si.setFormatComponent(formats);

        JButton rotate = new JButton("Rotate");
        rotate.setActionCommand("Rotate");
        rotate.addActionListener(si);
        rotate.setBounds(300, 300, 100, 40);
        rotate.setEnabled(true);
        panel.add(rotate);

        JButton crop = new JButton("Crop");
        crop.setActionCommand("Crop");
        crop.addActionListener(si);
        crop.setBounds(300, 300, 100, 40);
        crop.setEnabled(true);
        panel.add(crop);

        panel.add(choices);
        panel.add(new JLabel("Save As"));
        panel.add(formats);
        f.add("Center", si);
        f.add("South", panel);
        f.pack();
        f.setVisible(true);

    }

}
