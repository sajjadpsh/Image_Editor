package ir.ac.aut.filter;

import ir.ac.aut.graphics.GuiSetup;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.*;


import static com.sun.javafx.fxml.expression.Expression.add;

public class Rotate implements ActionListener {

    JSlider slider;
    JLabel label;
    BufferedImage image;

    public Rotate() {

        new GuiSetup().setLayout(new BorderLayout());
        label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setVerticalAlignment(JLabel.CENTER);

        //label.setIcon(new ImageIcon(image));

        new GuiSetup().add(label);

        slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(360);
        slider.setMinorTickSpacing(5);
        slider.setMajorTickSpacing(10);
        slider.setValue(0);
        add(slider, BorderLayout.CENTER);
    }



    public void actionPerformed(ActionEvent e) {
        double radians = Math.toRadians(slider.getValue());
        double sin = Math.abs(Math.sin(radians));
        double cos = Math.abs(Math.cos(radians));
        int newWidth = (int) Math.round(image.getWidth() * cos + image.getHeight() * sin);
        int newHeight = (int) Math.round(image.getWidth() * sin + image.getHeight() * cos);

        BufferedImage rotate = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotate.createGraphics();
        int x = (newWidth - image.getWidth()) / 2;
        int y = (newHeight - image.getHeight()) / 2;
        AffineTransform at = new AffineTransform();
        at.setToRotation(getAngle(), x + (image.getWidth() / 2), y + (image.getHeight() / 2));
        at.translate(x, y);
        g2d.setTransform(at);
        g2d.drawImage(image, 0, 0, (ImageObserver) Rotate.this);
        g2d.dispose();
        label.setIcon(new ImageIcon(rotate));
    }

    public double getAngle() {

        return Math.toRadians(slider.getValue());

    }


}
