package ir.ac.aut.filter;

import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class Crop extends JFrame{
    java.awt.Image image;
    int startx, starty;
    int stopx, stopy;
    int currentx, currenty;

    public Crop(java.awt.Image image) throws Exception {
//        addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent we) {
//                System.exit(1);
//            }
//        });

        getContentPane().setLayout(new BorderLayout());
        //image = new javax.swing.ImageIcon("C:\\Users\\Sajjad\\Pictures\\Camera Roll\\WIN_20150828_211715.JPG").getImage();

        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                startx = me.getX();
                starty = me.getY();
                stopx = stopy = 0;
                repaint();
            }

            public void mouseReleased(MouseEvent me) {
                stopx = me.getX();
                stopy = me.getY();
                currentx = currenty = 0;
                repaint();
                constructFrame(getCroppedImage(image, startx, starty,stopx-startx,stopy-starty));
            }
        });

        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent me) {
                currentx = me.getX();
                currenty = me.getY();
                repaint();
            }
        });
    }

    public void constructFrame(java.awt.Image img) {
        JFrame frame = new JFrame() {
            public void paint(Graphics g) {
                g.drawImage(img, 0, 15, null);
            }
        };
        frame.setSize(img.getWidth(null), img.getHeight(null));
        frame.setVisible(true);
        frame.invalidate();
    }

    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(image, 0, 0, this);

        if (startx != 0 && currentx != 0) {
            g2d.setColor(Color.white);
            BasicStroke bs = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                    5, new float[] {10}, 0);
            g2d.setStroke(bs);
            g2d.drawRect(startx, starty, currentx-startx, currenty-starty);
        }

    }

    public java.awt.Image getCroppedImage(java.awt.Image img, int x, int y, int w, int h) {
        CropImageFilter cif = new CropImageFilter(x, y, w, h);
        FilteredImageSource fis = new FilteredImageSource(img.getSource(), cif);
        java.awt.Image croppedImage = getToolkit().createImage(fis);

        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(croppedImage, 0);
        try {
            tracker.waitForID(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return croppedImage;
    }

//    public static void main(String []args) throws Exception {
//        Crop main = new Crop();
//        main.setSize(500, 400);
//        main.setVisible(true);
//    }
}
