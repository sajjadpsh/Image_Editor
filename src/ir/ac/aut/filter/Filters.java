package ir.ac.aut.filter;

import com.jhlabs.image.*;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.io.Serializable;
import java.util.Hashtable;

/**
 * Created by Sajjad on 5/24/2017.
 */
public class Filters {

    abstract class AbstractBufferedImageOp implements BufferedImageOp, Cloneable {
        public AbstractBufferedImageOp() {
        }

        public BufferedImage createCompatibleDestImage(BufferedImage src, ColorModel dstCM) {
            if (dstCM == null) {
                dstCM = src.getColorModel();
            }

            return new BufferedImage(dstCM, dstCM.createCompatibleWritableRaster(src.getWidth(), src.getHeight()), dstCM.isAlphaPremultiplied(), (Hashtable) null);
        }

        public Rectangle2D getBounds2D(BufferedImage src) {
            return new Rectangle(0, 0, src.getWidth(), src.getHeight());
        }

        public Point2D getPoint2D(Point2D srcPt, Point2D dstPt) {
            if (dstPt == null) {
                dstPt = new Point2D.Double();
            }

            ((Point2D) dstPt).setLocation(srcPt.getX(), srcPt.getY());
            return (Point2D) dstPt;
        }

        public RenderingHints getRenderingHints() {
            return null;
        }

        public int[] getRGB(BufferedImage image, int x, int y, int width, int height, int[] pixels) {
            int type = image.getType();
            return type != 2 && type != 1 ? image.getRGB(x, y, width, height, pixels, 0, width) : (int[]) ((int[]) image.getRaster().getDataElements(x, y, width, height, pixels));
        }

        public void setRGB(BufferedImage image, int x, int y, int width, int height, int[] pixels) {
            int type = image.getType();
            if (type != 2 && type != 1) {
                image.setRGB(x, y, width, height, pixels, 0, width);
            } else {
                image.getRaster().setDataElements(x, y, width, height, pixels);
            }

        }

        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException var2) {
                return null;
            }
        }

        /**
         * Created by Windows 10 on 02/06/2017.
         */

    }

    class CropFilter extends com.jhlabs.image.AbstractBufferedImageOp {
        private int x;
        private int y;
        private int width;
        private int height;

        public CropFilter() {
            this(0, 0, 32, 32);
        }

        public CropFilter(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getX() {
            return this.x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return this.y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getWidth() {
            return this.width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return this.height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public BufferedImage filter(BufferedImage src, BufferedImage dst) {
            int w = src.getWidth();
            int h = src.getHeight();
            if (dst == null) {
                ColorModel g = src.getColorModel();
                dst = new BufferedImage(g, g.createCompatibleWritableRaster(this.width, this.height), g.isAlphaPremultiplied(), (Hashtable) null);
            }

            Graphics2D g1 = dst.createGraphics();
            g1.drawRenderedImage(src, AffineTransform.getTranslateInstance((double) (-this.x), (double) (-this.y)));
            g1.dispose();
            return dst;
        }

        public String toString() {
            return "Distort/Crop";
        }
    }

    class ContourFilter extends WholeImageFilter {
        private float levels = 5.0F;
        private float scale = 1.0F;
        private float offset = 0.0F;
        private int contourColor = -16777216;

        public ContourFilter() {
        }

        public float getLevels() {
            return this.levels;
        }

        public void setLevels(float levels) {
            this.levels = levels;
        }

        public float getScale() {
            return this.scale;
        }

        public void setScale(float scale) {
            this.scale = scale;
        }

        public float getOffset() {
            return this.offset;
        }

        public void setOffset(float offset) {
            this.offset = offset;
        }

        protected int[] filterPixels(int width, int height, int[] inPixels, Rectangle transformedSpace) {
            int index = 0;
            short[][] r = new short[3][width];
            int[] outPixels = new int[width * height];
            short[] table = new short[256];
            int offsetl = (int) (this.offset * 256.0F / this.levels);

            int y;
            for (y = 0; y < 256; ++y) {
                table[y] = (short) PixelUtils.clamp((int) (255.0D * Math.floor((double) (this.levels * (float) (y + offsetl) / 256.0F)) / (double) (this.levels - 1.0F) - (double) offsetl));
            }

            for (y = 0; y < width; ++y) {
                int yIn = inPixels[y];
                r[1][y] = (short) PixelUtils.brightness(yIn);
            }

            for (y = 0; y < height; ++y) {
                boolean var26 = y > 0 && y < height - 1;
                int nextRowIndex = index + width;
                int t;
                if (y < height - 1) {
                    for (t = 0; t < width; ++t) {
                        int xIn = inPixels[nextRowIndex++];
                        r[2][t] = (short) PixelUtils.brightness(xIn);
                    }
                }

                for (t = 0; t < width; ++t) {
                    boolean var28 = t > 0 && t < width - 1;
                    int w = t - 1;
                    int e = t + 1;
                    int v = 0;
                    if (var26 && var28) {
                        short nwb = r[0][w];
                        short neb = r[0][t];
                        short swb = r[1][w];
                        short seb = r[1][t];
                        short nw = table[nwb];
                        short ne = table[neb];
                        short sw = table[swb];
                        short se = table[seb];
                        if (nw != ne || nw != sw || ne != se || sw != se) {
                            v = (int) (this.scale * (float) (Math.abs(nwb - neb) + Math.abs(nwb - swb) + Math.abs(neb - seb) + Math.abs(swb - seb)));
                            if (v > 255) {
                                v = 255;
                            }
                        }
                    }

                    if (v != 0) {
                        outPixels[index] = PixelUtils.combinePixels(inPixels[index], this.contourColor, 1, v);
                    } else {
                        outPixels[index] = inPixels[index];
                    }

                    ++index;
                }

                short[] var27 = r[0];
                r[0] = r[1];
                r[1] = r[2];
                r[2] = var27;
            }

            return outPixels;
        }

        public String toString() {
            return "Stylize/Contour...";
        }
    }


    class UnsharpFilter extends GaussianFilter {
        static final long serialVersionUID = 5377089073023183684L;
        private float amount = 0.5F;
        private int threshold = 1;

        public UnsharpFilter() {
            this.radius = 2.0F;
        }

        public int getThreshold() {
            return this.threshold;
        }

        public void setThreshold(int threshold) {
            this.threshold = threshold;
        }

        public float getAmount() {
            return this.amount;
        }

        public void setAmount(float amount) {
            this.amount = amount;
        }

        public BufferedImage filter(BufferedImage src, BufferedImage dst) {
            int width = src.getWidth();
            int height = src.getHeight();
            if (dst == null) {
                dst = this.createCompatibleDestImage(src, (ColorModel) null);
            }

            int[] inPixels = new int[width * height];
            int[] outPixels = new int[width * height];
            src.getRGB(0, 0, width, height, inPixels, 0, width);
            if (this.radius > 0.0F) {
                convolveAndTranspose(this.kernel, inPixels, outPixels, width, height, this.alpha, CLAMP_EDGES);
                convolveAndTranspose(this.kernel, outPixels, inPixels, height, width, this.alpha, CLAMP_EDGES);
            }

            src.getRGB(0, 0, width, height, outPixels, 0, width);
            float a = 4.0F * this.amount;
            int index = 0;

            for (int y = 0; y < height; ++y) {
                for (int x = 0; x < width; ++x) {
                    int rgb1 = outPixels[index];
                    int r1 = rgb1 >> 16 & 255;
                    int g1 = rgb1 >> 8 & 255;
                    int b1 = rgb1 & 255;
                    int rgb2 = inPixels[index];
                    int r2 = rgb2 >> 16 & 255;
                    int g2 = rgb2 >> 8 & 255;
                    int b2 = rgb2 & 255;
                    if (Math.abs(r1 - r2) >= this.threshold) {
                        r1 = PixelUtils.clamp((int) ((a + 1.0F) * (float) (r1 - r2) + (float) r2));
                    }

                    if (Math.abs(g1 - g2) >= this.threshold) {
                        g1 = PixelUtils.clamp((int) ((a + 1.0F) * (float) (g1 - g2) + (float) g2));
                    }

                    if (Math.abs(b1 - b2) >= this.threshold) {
                        b1 = PixelUtils.clamp((int) ((a + 1.0F) * (float) (b1 - b2) + (float) b2));
                    }

                    inPixels[index] = rgb1 & -16777216 | r1 << 16 | g1 << 8 | b1;
                    ++index;
                }
            }

            dst.setRGB(0, 0, width, height, inPixels, 0, width);
            return dst;
        }

        public String toString() {
            return "Blur/Unsharp Mask...";
        }
    }


    class MaskFilter extends PointFilter implements Serializable {
        private int mask;

        public MaskFilter() {
            this(-16711681);
        }

        public MaskFilter(int mask) {
            this.canFilterIndexColorModel = true;
            this.setMask(mask);
        }

        public int getMask() {
            return this.mask;
        }

        public void setMask(int mask) {
            this.mask = mask;
        }

        public int filterRGB(int x, int y, int rgb) {
            return rgb & this.mask;
        }

        public String toString() {
            return "Mask";
        }
    }

    class CrystallizeFilter extends CellularFilter {
        private float edgeThickness = 0.4F;
        private boolean fadeEdges = false;
        private int edgeColor = -16777216;

        public CrystallizeFilter() {
            this.setScale(16.0F);
            this.setRandomness(0.0F);
        }

        public float getEdgeThickness() {
            return this.edgeThickness;
        }

        public void setEdgeThickness(float edgeThickness) {
            this.edgeThickness = edgeThickness;
        }

        public boolean getFadeEdges() {
            return this.fadeEdges;
        }

        public void setFadeEdges(boolean fadeEdges) {
            this.fadeEdges = fadeEdges;
        }

        public int getEdgeColor() {
            return this.edgeColor;
        }

        public void setEdgeColor(int edgeColor) {
            this.edgeColor = edgeColor;
        }

        public int getPixel(int x, int y, int[] inPixels, int width, int height) {
            float nx = this.m00 * (float) x + this.m01 * (float) y;
            float ny = this.m10 * (float) x + this.m11 * (float) y;
            nx /= this.scale;
            ny /= this.scale * this.stretch;
            nx += 1000.0F;
            ny += 1000.0F;
            this.evaluate(nx, ny);
            float f1 = this.results[0].distance;
            float f2 = this.results[1].distance;
            int srcx = ImageMath.clamp((int) ((this.results[0].x - 1000.0F) * this.scale), 0, width - 1);
            int srcy = ImageMath.clamp((int) ((this.results[0].y - 1000.0F) * this.scale), 0, height - 1);
            int v = inPixels[srcy * width + srcx];
            float f = (f2 - f1) / this.edgeThickness;
            f = ImageMath.smoothStep(0.0F, this.edgeThickness, f);
            if (this.fadeEdges) {
                srcx = ImageMath.clamp((int) ((this.results[1].x - 1000.0F) * this.scale), 0, width - 1);
                srcy = ImageMath.clamp((int) ((this.results[1].y - 1000.0F) * this.scale), 0, height - 1);
                int v2 = inPixels[srcy * width + srcx];
                v2 = ImageMath.mixColors(0.5F, v2, v);
                v = ImageMath.mixColors(f, v2, v);
            } else {
                v = ImageMath.mixColors(f, this.edgeColor, v);
            }

            return v;
        }

        public String toString() {
            return "Stylize/Crystallize...";
        }
    }

    class InvertFilter extends PointFilter {
        public InvertFilter() {
            this.canFilterIndexColorModel = true;
        }

        public int filterRGB(int x, int y, int rgb) {
            int a = rgb & -16777216;
            return a | ~rgb & 16777215;
        }

        public String toString() {
            return "Colors/Invert";
        }
    }

    class TwirlFilter extends TransformFilter {
        static final long serialVersionUID = 1550445062822803342L;
        private float angle = 0.0F;
        private float centreX = 0.5F;
        private float centreY = 0.5F;
        private float radius = 100.0F;
        private float radius2 = 0.0F;
        private float icentreX;
        private float icentreY;

        public TwirlFilter() {
            this.setEdgeAction(1);
        }

        public float getAngle() {
            return this.angle;
        }

        public void setAngle(float angle) {
            this.angle = angle;
        }

        public float getCentreX() {
            return this.centreX;
        }

        public void setCentreX(float centreX) {
            this.centreX = centreX;
        }

        public float getCentreY() {
            return this.centreY;
        }

        public void setCentreY(float centreY) {
            this.centreY = centreY;
        }

        public Point2D getCentre() {
            return new Point2D.Float(this.centreX, this.centreY);
        }

        public void setCentre(Point2D centre) {
            this.centreX = (float) centre.getX();
            this.centreY = (float) centre.getY();
        }

        public float getRadius() {
            return this.radius;
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public BufferedImage filter(BufferedImage src, BufferedImage dst) {
            this.icentreX = (float) src.getWidth() * this.centreX;
            this.icentreY = (float) src.getHeight() * this.centreY;
            if (this.radius == 0.0F) {
                this.radius = Math.min(this.icentreX, this.icentreY);
            }

            this.radius2 = this.radius * this.radius;
            return super.filter(src, dst);
        }

        protected void transformInverse(int x, int y, float[] out) {
            float dx = (float) x - this.icentreX;
            float dy = (float) y - this.icentreY;
            float distance = dx * dx + dy * dy;
            if (distance > this.radius2) {
                out[0] = (float) x;
                out[1] = (float) y;
            } else {
                distance = (float) Math.sqrt((double) distance);
                float a = (float) Math.atan2((double) dy, (double) dx) + this.angle * (this.radius - distance) / this.radius;
                out[0] = this.icentreX + distance * (float) Math.cos((double) a);
                out[1] = this.icentreY + distance * (float) Math.sin((double) a);
            }

        }

        public String toString() {
            return "Distort/Twirl...";
        }
    }


    class ExposureFilter extends TransferFilter {
        private float exposure = 1.0F;

        public ExposureFilter() {
        }

        protected float transferFunction(float f) {
            return 1.0F - (float) Math.exp((double) (-f * this.exposure));
        }

        public float getExposure() {
            return this.exposure;
        }

        public void setExposure(float exposure) {
            this.exposure = exposure;
            this.initialized = false;
        }

        public String toString() {
            return "Colors/Exposure...";
        }
    }


    class LookupFilter extends PointFilter {
        private Colormap colormap = new Gradient();

        public LookupFilter() {
            this.canFilterIndexColorModel = true;
        }

        public LookupFilter(Colormap colormap) {
            this.canFilterIndexColorModel = true;
            this.colormap = colormap;
        }

        public Colormap getColormap() {
            return this.colormap;
        }

        public void setColormap(Colormap colormap) {
            this.colormap = colormap;
        }

        public int filterRGB(int x, int y, int rgb) {
            int r = rgb >> 16 & 255;
            int g = rgb >> 8 & 255;
            int b = rgb & 255;
            rgb = (r + g + b) / 3;
            return this.colormap.getColor((float) rgb / 255.0F);
        }

        public String toString() {
            return "Colors/Lookup...";
        }
    }

    class BorderFilter extends com.jhlabs.image.AbstractBufferedImageOp {
        private int leftBorder;
        private int rightBorder;
        private int topBorder;
        private int bottomBorder;
        private Paint borderPaint;

        public BorderFilter() {
        }

        public BorderFilter(int leftBorder, int topBorder, int rightBorder, int bottomBorder, Paint borderPaint) {
            this.leftBorder = leftBorder;
            this.topBorder = topBorder;
            this.rightBorder = rightBorder;
            this.bottomBorder = bottomBorder;
            this.borderPaint = borderPaint;
        }

        public int getLeftBorder() {
            return this.leftBorder;
        }

        public void setLeftBorder(int leftBorder) {
            this.leftBorder = leftBorder;
        }

        public int getRightBorder() {
            return this.rightBorder;
        }

        public void setRightBorder(int rightBorder) {
            this.rightBorder = rightBorder;
        }

        public int getTopBorder() {
            return this.topBorder;
        }

        public void setTopBorder(int topBorder) {
            this.topBorder = topBorder;
        }

        public int getBottomBorder() {
            return this.bottomBorder;
        }

        public void setBottomBorder(int bottomBorder) {
            this.bottomBorder = bottomBorder;
        }

        public BufferedImage filter(BufferedImage src, BufferedImage dst) {
            int width = src.getWidth();
            int height = src.getHeight();
            if (dst == null) {
                dst = new BufferedImage(width + this.leftBorder + this.rightBorder, height + this.topBorder + this.bottomBorder, src.getType());
            }

            Graphics2D g = dst.createGraphics();
            if (this.borderPaint != null) {
                g.setPaint(this.borderPaint);
                if (this.leftBorder > 0) {
                    g.fillRect(0, 0, this.leftBorder, height);
                }

                if (this.rightBorder > 0) {
                    g.fillRect(width - this.rightBorder, 0, this.rightBorder, height);
                }

                if (this.topBorder > 0) {
                    g.fillRect(this.leftBorder, 0, width - this.leftBorder - this.rightBorder, this.topBorder);
                }

                if (this.bottomBorder > 0) {
                    g.fillRect(this.leftBorder, height - this.bottomBorder, width - this.leftBorder - this.rightBorder, this.bottomBorder);
                }
            }

            g.drawRenderedImage(src, AffineTransform.getTranslateInstance((double) this.leftBorder, (double) this.rightBorder));
            g.dispose();
            return dst;
        }

        public String toString() {
            return "Distort/Border...";
        }
    }

}
