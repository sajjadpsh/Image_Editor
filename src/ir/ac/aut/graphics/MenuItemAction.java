package ir.ac.aut.graphics;

/**
 * Created by Sajjad on 5/18/2017.
 */
import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by Sajjad on 5/15/2017.
 */
public class MenuItemAction extends AbstractAction {

    public MenuItemAction(String text, Integer mnemonic) {
        super(text);

        putValue(MNEMONIC_KEY, mnemonic);
    }




    @Override
    public void actionPerformed(ActionEvent e) {

        System.out.println(e.getActionCommand());
    }

}