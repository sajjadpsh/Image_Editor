package ir.ac.aut.graphics;


import ir.ac.aut.*;
import ir.ac.aut.Image;
import ir.ac.aut.image.OpenImage;
import ir.ac.aut.image.SaveImage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;


public class GuiSetup extends JFrame {

    JMenuBar menuBar = new JMenuBar();
    JMenu fileMenu, editMenu;
    JMenuItem newImage, openImage, saveImage, exit, addText, addSticker;


    public GuiSetup() {
        initUI();
    }

    public void initUI() {

        createMenuBar();
        setTitle("Java Simple ir.ac.aut.Image Editor");
        setSize((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(), (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void createMenuBar() {

        fileMenu = new JMenu("File");
        editMenu = new JMenu("Edit");

        newImage = new JMenuItem(new MenuItemAction("New", KeyEvent.VK_N));
        newImage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        openImage = new JMenuItem(new MenuItemAction("Open", KeyEvent.VK_O));
        openImage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        saveImage = new JMenuItem(new MenuItemAction("Save", KeyEvent.VK_S));
        saveImage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        exit = new JMenuItem(new MenuItemAction("Exit", KeyEvent.VK_W));
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));

        addText = new JMenuItem(new MenuItemAction("Add Text", KeyEvent.VK_T));
        addText.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
        addSticker = new JMenuItem(new MenuItemAction("Add Sticker", KeyEvent.VK_E));
        addSticker.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));

        fileMenu.add(newImage);
        fileMenu.add(openImage);
        fileMenu.add(saveImage);
        fileMenu.addSeparator();
        fileMenu.add(exit);

        editMenu.add(addText);
        editMenu.add(addSticker);

        menuBar.add(fileMenu);
        menuBar.add(editMenu);

        setJMenuBar(menuBar);


        action();
    }

    public void action() {


        newImage.addActionListener((ActionEvent n) -> {

        });

        openImage.addActionListener((ActionEvent o) -> {

            new OpenImage(this);
        });

        saveImage.addActionListener((ActionEvent s) -> {

        });

        exit.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
    }

}
