package ir.ac.aut;


import com.jhlabs.image.*;

import ir.ac.aut.filter.Crop;
import ir.ac.aut.filter.Rotate;
import ir.ac.aut.graphics.MenuItemAction;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.TreeSet;

import static jdk.nashorn.internal.runtime.regexp.joni.Syntax.Java;

public class Image extends Component implements ActionListener {

    String filter[] = {"Orginal", "TwirlFilter", "EdgeFilter", "PlasmaFilter", "CrystallizeFilter",
            "InvertFilter", "BlurFilter", "GrayscaleFilter", "MaskFilter", "SolarizeFilter", "ChannelMixFilter"};
    int opIndex;
    BufferedImage bi;
    BufferedImage biFiltered;
    int w, h;
    int lastOp;


    public Image(String path) {

        try {
            bi = ImageIO.read(new File(path));
            w = bi.getWidth(null);
            h = bi.getHeight(null);
            if (bi.getType() != BufferedImage.TYPE_INT_RGB) {
                BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
                Graphics big = bi2.getGraphics();
                big.drawImage(bi, 0, 0, null);
                biFiltered = bi = bi2;
            }
        } catch (IOException e) {
            System.out.println("ir.ac.aut.Image could not be read");
            System.exit(1);
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(w, h);
    }

    public String[] getDescriptions() {
        return filter;
    }

    void setOpIndex(int i) {
        opIndex = i;
    }

    public void paint(Graphics g) {
        filterImage();
        g.drawImage(biFiltered, 0, 0, null);
    }

    private BufferedImage filterImage() {

        if (opIndex == lastOp) {
            return null;
        }
        lastOp = opIndex;
        switch (opIndex) {

            case 0:
                biFiltered.flush();
                repaint();
            case 1:
                TwirlFilter twirlFilter = new TwirlFilter();
                return twirlFilter.filter(bi, biFiltered);
            case 2:
                EdgeFilter edgeFilter = new EdgeFilter();
                return edgeFilter.filter(bi, biFiltered);
            case 3:
                PlasmaFilter plasmaFilter = new PlasmaFilter();
                return plasmaFilter.filter(bi, biFiltered);
            case 4:
                CrystallizeFilter crystallizeFilter = new CrystallizeFilter();
                return crystallizeFilter.filter(bi, biFiltered);
            case 5:
                InvertFilter invertFilter = new InvertFilter();
                return invertFilter.filter(bi, biFiltered);
            case 6:
                BlurFilter blurFilter = new BlurFilter();
                return blurFilter.filter(bi, biFiltered);
            case 7:
                GrayscaleFilter grayscaleFilter = new GrayscaleFilter();
                return grayscaleFilter.filter(bi, biFiltered);
            case 8:
                MaskFilter maskFilter = new MaskFilter();
                return maskFilter.filter(bi, biFiltered);
            case 9:
                SolarizeFilter solarizeFilter = new SolarizeFilter();
                return solarizeFilter.filter(bi, biFiltered);
            case 10:
                ChannelMixFilter channelMixFilter = new ChannelMixFilter();
                return channelMixFilter.filter(bi, biFiltered);

        }

        biFiltered = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        return biFiltered;
    }

    public String[] getFormats() {
        String[] formats = ImageIO.getWriterFormatNames();
        TreeSet<String> formatSet = new TreeSet<String>();
        for (String s : formats) {
            formatSet.add(s.toLowerCase());
        }
        return formatSet.toArray(new String[0]);
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();

        if (command.equalsIgnoreCase("SetFilter")) {

            setOpIndex(choices.getSelectedIndex());
            repaint();
        }
        if (command.equalsIgnoreCase("Formats")) {
            int opt = JOptionPane.showConfirmDialog(this, "Do you want to save it?", "save image",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null);

            switch (opt) {
                case JOptionPane.CANCEL_OPTION:
                    return;
            }

            String format = (String) formats.getSelectedItem();

            File saveFile = new File("savedimage." + format);
            JFileChooser chooser = new JFileChooser();
            chooser.setSelectedFile(saveFile);
            int rval = chooser.showSaveDialog(formats);
            if (rval == JFileChooser.APPROVE_OPTION) {
                saveFile = chooser.getSelectedFile();

                try {
                    ImageIO.write(biFiltered, format, saveFile);
                } catch (IOException ignored) {
                }
            }
        }
        if (command.equalsIgnoreCase("Rotate")){
            new Rotate();
        }
        if (command.equalsIgnoreCase("Crop")){
            try {
                new Crop(bi);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }


    private JComboBox<String> choices;
    private JComboBox<String> formats;

    public void setFormatComponent(final JComboBox<String> formats) {
        this.formats = formats;
    }

    public void setChoiceComponent(final JComboBox<String> choices) {
        this.choices = choices;
    }
}